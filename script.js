$(function () {
    $("#hideMe").dblclick(function() {
        $(this).hide();
    });

    $("#enterMe").mouseenter(function() {
        alert("You entered me!");
    });

    $("#leaveMe").mouseleave(function() {
        alert("You left me! Bye!");
    });

    $("#enterLeaveMe").hover(function() {
        alert("You entered me!");
        // $(this).css("background-color", "#cccccc");
    },
    function() {
        alert("You left me! Bye!");
    });

    $("input").focus(function () {
        $(this).css("background-color", "#C04848");
    });

    $("input").blur(function() {
        $(this).css("background-color", "#480048");
    });

    $("#changeColor").on({
        mouseenter: function() {
            $(this).css("background-color", "#C04848");
        },
        mouseleave: function() {
            $(this).css("background-color", "#ED4264");
        },
        click: function() {
            $(this).css("background-color", "#8E54E9");
        }

    })


});


